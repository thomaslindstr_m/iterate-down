# iterate-down

[![build status](https://gitlab.com/thomaslindstr_m/iterate-down/badges/master/build.svg)](https://gitlab.com/thomaslindstr_m/iterate-down/commits/master)

while loop abstraction for iterating down with -1 as increment

```
npm install @amphibian/iterate-down
```

```javascript
var iterateDown = require('@amphibian/iterate-down');

// Iterate 5 times from 5
iterateDown(5, function (i) {
    console.log(i); // 4, 3, 2, 1, 0
});

// Iterate 5 times, but stop at 1
var question = iterateDown(5, function (i, end) {
    if (i === 1) {
        end('hola que tal');
    }

    console.log(i); // 4, 3, 2
});

console.log(question); // > hola que hora es
```
